# Google Analytics Vimeo

## Summary

An easy-to-use solution to track Vimeo Player API events with the Event
Tracking method of Google Analytics. The plugin supports Universal Analytics,
Classic Google Analytics, and Google Tag Manager.

### How to install

#### The non-composer way

1. Download this module
2. [Download Google Analytics Vimeo Video Tracking](https://github.com/sanderheilbron/vimeo.ga.js) library and place it in the
   libraries folder.
   Google Analytics Vimeo Video Tracking library folder name
   should be 'vimeo.ga.js'.
3. Install google_analytics_vimeo the [usual way](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).
4. Configure Google Analytics Vimeo Video Tracking in
   /admin/config/googleanalytics_vimeo/settings page.

#### The composer way 1

Run `composer require wikimedia/composer-merge-plugin`

Update the root `composer.json` file. For example:

```
   "extra": {
       "merge-plugin": {
           "include": [
               "web/modules/contrib/google_analytics_vimeo/composer.libraries.json"
           ]
       }
   }
```


Run `composer require drupal/google_analytics_vimeo sanderheilbron/vimeo.ga.js`, the Google Analytics Vimeo Video Tracking library will be
installed to the `libraries` folder automatically.

#### The composer way 2

Copy the following into the root `composer.json` file's `repository` key

```
    "repositories": [
        {
            "type": "package",
            "package": {
                "name": "sanderheilbron/vimeo.ga.js",
                "version": "0.6",
                "type": "drupal-library",
                "dist": {
                    "url": "https://github.com/sanderheilbron/vimeo.ga.js/archive/refs/tags/0.6.zip",
                    "type": "zip"
                }
            }
        }
    ]
```

Run `composer require drupal/google_analytics_vimeo sanderheilbron/vimeo.ga.js`, the Google Analytics Vimeo Video Tracking library
will be installed to the `libraries` folder automatically as well.

## Features

1. Easy integration with Google Analytics Vimeo Video Tracking plugin.
2. Detailed configuration page to manage different attributes of
   Google Analytics Vimeo Video Tracking plugin.
3. Tracking different events like 25% Progress, 50% Progress,
   75% Progress, Completed video.
4. Also, tracking Started video, Paused video, Resumed video,
   Skipped video events.
